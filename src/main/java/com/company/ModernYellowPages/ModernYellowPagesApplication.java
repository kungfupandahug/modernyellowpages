package com.company.ModernYellowPages;

import com.company.ModernYellowPages.DBhandler.Connect;
import com.mysql.cj.conf.ConnectionPropertiesTransform;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class ModernYellowPagesApplication {

    static private Scanner input;
    static private Connect connect;
    static private String first_name;
    static private String last_name;
    static private String homeaddr;
    static private String dob;
    static private String phonenum;
    static private String emailaddr;

    public static void main(String[] args) {
        // TODO: un-comment when starting with webapps..
        //SpringApplication.run(ModernYellowPagesApplication.class, args);

        printMeny();

        System.out.println("Your number: ");

        connect = new Connect();
        input = new Scanner(System.in);
        String choose = input.next();
        switch (choose) {
            case "1":
                insertPerson();
                break;
            case "2":
                updatePerson();
                break;
            case "3":
                deletePerson();
                break;
            case "4":
                searchPerson();
                break;
            case "5":
            	displayFamily();
                break;
			case "6":
				break;
            default:
                System.out.println("You did not choose a number! :((");
        }
    }

    public static void printMeny() {
        System.out.println("Please choose one of the numbers below: ");
        System.out.println("\t" + "[1] Insert a person");
        System.out.println("\t" + "[2] Update a person");
        System.out.println("\t" + "[3] Delete a person");
        System.out.println("\t" + "[4] Search for a person");
        System.out.println("\t" + "[5] Display related family");
        System.out.println("\t" + "[6] Exit");
    }


    public static void insertPerson() {
        System.out.println("Please provide information about the person you want to insert:");
        System.out.println("\t" + "First name: ");
        first_name = input.next();
        System.out.println("\t" + "Last name: ");
        last_name = input.next();
        System.out.println("\t" + "Address: ");
        homeaddr = input.next();
        System.out.println("\t" + "Date of birth: ");
        dob = input.next();
        System.out.println("\t" + "Phone number: ");
        phonenum = input.next();
        System.out.println("\t" + "Mail address: ");
        emailaddr = input.next();

		connect.insertIntoTest(first_name, last_name, homeaddr, dob, phonenum, emailaddr);
    }

    public static void updatePerson() {
        System.out.println("Please provide information about the person you want to update:");
        System.out.println("\t" + "First name: ");
        first_name = input.next();
        System.out.println("\t" + "Last name: ");
        last_name = input.next();
        System.out.println("\t" + "Address: ");
        homeaddr = input.next();
        System.out.println("\t" + "Date of birth: ");
        dob = input.next();

        System.out.println("Write in one of these update options: ");
        System.out.println("\t" + "[1] Change the persons first name");
        System.out.println("\t" + "[2] Change the persons last name");
        System.out.println("\t" + "[3] Change the persons home address");
        System.out.println("\t" + "[4] Change the persons date of birth");
        System.out.println("\t" + "[5] Change the persons email address");
        System.out.println("\t" + "[6] Change the persons family relationship");
        System.out.println("\t" + "[7] Exit");
        int choose = Integer.parseInt(input.next());

        System.out.println("Type in what you want to update it to:");
        String update = input.next();
        String updateline = "";

        switch (choose) {
            case 1:
                updateline = "first_name " + update;
                break;
            case 2:
                updateline = "last_name " + update;
                break;
            case 3:
                updateline = "homeaddr " + update;
                break;
            case 4:
                updateline = "dob " + update;
                break;
            case 5:
                updateline = "phone_num " + update;
                break;
            case 6:
                updateline = "emailaddr " + update;
                break;
            case 7:
                updateline = "title " + update;
                break;
            default:
                System.out.println("You didn't choose any number!");
        }
        if (choose > 0 && choose < 8) { // If the choice was valid
            connect.updateTable(first_name, last_name, homeaddr, dob, updateline);
        }
    }

    public static void deletePerson() {
        System.out.println("Please provide information about the person you want to delete:");
        System.out.println("\t" + "First name: ");
        first_name = input.next();
        System.out.println("\t" + "Last name: ");
        last_name = input.next();
        System.out.println("\t" + "Adress: ");
        homeaddr = input.next();
        System.out.println("\t" + "Date of birth: ");
        dob = input.next();

        connect.deletePerson(first_name, last_name, homeaddr, dob);
        System.out.println("Successfully deleted " + first_name + " " + last_name);

    }

    public static void searchPerson() {
        System.out.println("Pick an option to search by: ");
        System.out.println("[1] First name \n[2] Last name \n[3] Telephone number");
        int num = Integer.parseInt(input.next());
        System.out.println("Type in your search word: ");
        String searchword = input.next();
        String line = "";

        switch (num) {
            case 1:
                line = "first_name " + searchword;
                break;

            case 2:
                line = "last_name " + searchword;
                break;

            case 3:
                line = "telephone_num " + searchword;
                break;

            default:
                System.out.println("You didn't choose any numbers!");
                break;
        }

        if (num > 0 && num < 4) { //Execute if we didn't jump into default
            connect.searchFrom(line);
        }
    }

    public static void displayFamily() {
		System.out.println("Please provide information about the person you want to find related family for:");
		System.out.println("\t" + "First name: ");
		first_name = input.next();
		System.out.println("\t" + "Last name: ");
		last_name = input.next();
		System.out.println("\t" + "Address: ");
		homeaddr = input.next();
		System.out.println("\t" + "Date of birth: ");
		dob = input.next();

		connect.displayRelatedFamily(first_name, last_name, homeaddr, dob);
	}
}
