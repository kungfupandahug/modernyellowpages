package com.company.ModernYellowPages.DBhandler;

import com.mysql.cj.protocol.Resultset;

import java.sql.*;
import java.io.*;
import java.util.*;

public class Connect {

    private Connection connect() {

        Connection con = null;
        try {
            con = DriverManager.getConnection("jdbc:mysql://remotemysql.com/BJTtLMZYjr?autoReconnect=true&useSSL=false", "BJTtLMZYjr", "H1VhycoXdk");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return con;
    }

    public void createTableTest() {

        String sqlCreatePerson = "CREATE TABLE IF NOT EXISTS Person" +
                "(person_id INT(6) UNSIGNED AUTO_INCREMENT," +
                "first_name VARCHAR(30)," +
                "last_name VARCHAR(30)," +
                "homeaddr VARCHAR(128)," +
                "dob VARCHAR(30)," +
                "PRIMARY KEY(person_id, first_name, last_name, homeaddr, dob))";

        String sqlCreatePhone = "CREATE TABLE IF NOT EXISTS Phone" +
                "(phone_id INT(6) UNSIGNED AUTO_INCREMENT," +
                "phone_num VARCHAR(30)," +
                "person_id INT(6) UNSIGNED," +
                "PRIMARY KEY(phone_id, phone_num, person_id)," +
                "FOREIGN KEY (person_id) REFERENCES Person(person_id))";

        String sqlCreateEmail = "CREATE TABLE IF NOT EXISTS Email" +
                "(email_id INT(6) UNSIGNED AUTO_INCREMENT," +
                "emailaddr VARCHAR(128)," +
                "person_id INT(6) UNSIGNED," +
                "PRIMARY KEY(email_id, emailaddr, person_id)," +
                "FOREIGN KEY (person_id) REFERENCES Person(person_id))";


        String sqlCreateFamily = "CREATE TABLE IF NOT EXISTS Family" +
                "(person_id INT(6) UNSIGNED," +
                "relation_id INT(6) UNSIGNED," +
                "title VARCHAR(30) NOT NULL CHECK (title IN ('FATHER', 'MOTHER', 'SISTER', 'BROTHER'))," +
                "PRIMARY KEY(person_id, relation_id)," +
                "FOREIGN KEY (person_id) REFERENCES Person(person_id)," +
                "FOREIGN KEY (relation_id) REFERENCES Person(person_id))";

        try (
                Connection con = this.connect();
                Statement stmt = con.createStatement()
        ) {

            stmt.execute(sqlCreatePerson);
            System.out.println("Table <Person> created..");

            stmt.execute(sqlCreatePhone);
            System.out.println("Table <Phone> created..");

            stmt.execute(sqlCreateEmail);
            System.out.println("Table <Email> created..");

            stmt.execute(sqlCreateFamily);
            System.out.println("Table <Family> created..");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertIntoTest(String fName, String lName, String addr, String dob, String phonenum, String emailaddr) {

        String sqlInsertPerson = "INSERT INTO Person(first_name, last_name, homeaddr, dob)" +
                "VALUES(?, ?, ?, ?)";

        try (
                Connection con = this.connect();
        ) {

            PreparedStatement pstmt = con.prepareStatement(sqlInsertPerson);
            pstmt.setString(1, fName);
            pstmt.setString(2, lName);
            pstmt.setString(3, addr);
            pstmt.setString(4, dob);

            pstmt.executeUpdate();
            System.out.println("Inserted into table <Person>..");

            if (phonenum == null && emailaddr == null)
                return;

            int person_id = selectFromPerson(fName, lName, addr, dob);

            if (phonenum != null) {
                String sqlInsertPhone = "INSERT INTO Phone(phone_num, person_id)" +
                        "VALUES(?, ?)";
                pstmt = con.prepareStatement(sqlInsertPhone);
                pstmt.setString(1, phonenum);
                pstmt.setInt(2, person_id);

                pstmt.executeUpdate();
                System.out.println("Inserted into table <Phone>");
            }

            if (emailaddr != null) {
                String sqlInsertEmail = "INSERT INTO Email(emailaddr, person_id)" +
                        "VALUES(?, ?)";
                pstmt = con.prepareStatement(sqlInsertEmail);
                pstmt.setString(1, emailaddr);
                pstmt.setInt(2, person_id);

                pstmt.executeUpdate();
                System.out.println("Inserted into table <Email>");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private int selectFromPerson(String fName, String lName, String addr, String dob) {

        String sqlSelect = "SELECT person_id FROM Person " +
                "WHERE first_name=? AND last_name=? AND homeaddr=? AND dob=?";

        try (
                Connection con = this.connect();
                PreparedStatement pstmt = con.prepareStatement(sqlSelect);
        ) {

            pstmt.setString(1, fName);
            pstmt.setString(2, lName);
            pstmt.setString(3, addr);
            pstmt.setString(4, dob);

            ResultSet rs = pstmt.executeQuery();

            System.out.println("Selecting person_id from table..");

            if (rs.next())
                return rs.getInt("person_id");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return -1;
    }

    public void selectAllTest() {

        String sqlSelect = "SELECT * FROM Person";

        try (
                Connection con = this.connect();
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(sqlSelect)
        ) {

            System.out.println("Selecting all from table..");
            while (rs.next()) {
                System.out.printf("%d: %s %s\n",
                        rs.getInt("person_id"),
                        rs.getString("first_name"),
                        rs.getString("last_name")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void dropTableTest() {
        String sqlDropPerson = "DROP TABLE IF EXISTS Person";
        String sqlDropPhone = "DROP TABLE IF EXISTS Phone";
        String sqlDropEmail = "DROP TABLE IF EXISTS Email";
        String sqlDropFamily = "DROP TABLE IF EXISTS Family";

        try (
                Connection con = this.connect();
                Statement stmt = con.createStatement()
        ) {

            // DONT UNCOMMENT -> INTHU WILL FIGHT YOU
            /*
            stmt.executeUpdate(sqlDropPhone);
            System.out.println("Table <Phone> dropped..");

            stmt.executeUpdate(sqlDropEmail);
            System.out.println("Table <Email> dropped..");

            stmt.executeUpdate(sqlDropFamily);
            System.out.println("Table <Family> dropped..");

            stmt.executeUpdate(sqlDropPerson);
            System.out.println("Table <Person> dropped..");

             */

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Method to update single elements in the database provided that we get the correct information back
    public void updateTable(String firstName, String lastName, String homeAddress, String dob, String updateElement) {
        int personId = selectFromPerson(firstName, lastName, homeAddress, dob);
        if (personId == -1) { //Jumps out of the method if the person we're trying to update doesn't exist
            System.out.println("The person does not exist cannot update their information. Typo?");
            return;
        }
        System.out.println("Updating information in the database..");
        String[] element = updateElement.split(" ");
        // element[0] --> where you want to update
        // element[1] --> what you want it to update to
        String updatePositionSql = "";
        PreparedStatement pstmt = null;
        int rowsAffected = 0;
        try (
                Connection con = this.connect();
        ) {
            switch (element[0]) {
                case "first_name":
                    updatePositionSql = "UPDATE Person SET first_name=? WHERE person_id=?";
                    break;

                case "last_name":
                    updatePositionSql = "UPDATE Person SET last_name=? WHERE person_id=?";
                    break;

                case "homeaddr":
                    updatePositionSql = "UPDATE Person SET homeaddr=? WHERE person_id=?";
                    break;

                case "":
                    updatePositionSql = "UPDATE Person SET dob=? WHERE person_id=?";
                    break;

                case "phone_num":
                    updatePositionSql = "UPDATE Phone SET phone_num=? WHERE person_id=? AND phone_id=?";
                    break;


                case "emailaddr":
                    updatePositionSql = "UPDATE Email SET emailaddr=? WHERE person_id=? AND email_id=?";
                    break;

                case "title":
                    updatePositionSql = "UPDATE Family SET title=? WHERE person_id=? AND relation_id=?";
                    break;
            }

            //
            if (element[0].equals("first_name") || element[0].equals("last_name") || element[0].equals("homeaddr") || element[0].equals("dob")) {
                pstmt = con.prepareStatement(updatePositionSql);
                pstmt.setString(1, element[1]);
                pstmt.setInt(2, personId);
                rowsAffected = pstmt.executeUpdate();
            } else if (element[0].equals("title") || element[0].equals("emailaddr") || element[0].equals("phone_num")) {
                pstmt = con.prepareStatement(updatePositionSql);
                pstmt.setString(1, element[1]);
                pstmt.setInt(2, personId);
                pstmt.setInt(3, Integer.parseInt(element[2]));
                rowsAffected = pstmt.executeUpdate();
            }

            if (rowsAffected < 1) {
                System.out.println("The update line is incorrect thus nothing was updated");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deletePerson(String fName, String lName, String addr, String dob) {
        try (
                Connection con = this.connect();
                Statement stmt = con.createStatement()
        ) {
            int person_id = selectFromPerson(fName, lName, addr, dob);

            String deletePerson = "DELETE FROM Person WHERE person_id = " + person_id;
            stmt.execute(deletePerson);
        } catch (SQLException e) {
            System.out.println("Could not find person");
            e.printStackTrace();
        }
    }

    public void searchFrom(String searchline) {
        String[] token = searchline.split(" ");
        //System.out.println(token[0] + "\n" + token[1] + "\n");

        String sqlSelect = "";
        switch (token[0]) {
            case "first_name":
                sqlSelect = "SELECT * FROM Person WHERE first_name='" + token[1] + "'";
                break;

            case "last_name":
                sqlSelect = "SELECT * FROM Person WHERE last_name='" + token[1] + "'";
                break;

            case "telephone_num":
                sqlSelect = "SELECT * FROM Person INNER JOIN Phone ON Person.person_id = Phone.person_id WHERE phone_num=" + token[1];
                break;
        }

        try (
                Connection con = this.connect();
                Statement stmt = con.createStatement()
        ) {

            ResultSet rs = stmt.executeQuery(sqlSelect);
            System.out.println("Selecting person from table..");
            while (rs.next()) {
                System.out.println(
                        "Name: " + rs.getString("first_name") + "\n" +
                                "Surname: " + rs.getString("last_name") + "\n" +
                                "Homeaddress: " + rs.getString("homeaddr") + "\n" +
                                "Date of birth: " + rs.getString("dob"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void displayRelatedFamily(String fname, String lname, String homeaddr, String dob) {
        HashMap<Integer, String> relationlist = new HashMap<>();
        int personId = selectFromPerson(fname, lname, homeaddr, dob);
        if (personId == -1) { //Jumps out of the method if the person we're trying to update doesn't exist
            System.out.println("The person does not exist cannot update their information. Typo?");
            return;
        }
        //sqlSelect = "SELECT * FROM Person INNER JOIN Phone ON Person.person_id = Phone.person_id WHERE phone_num=" + token[1];

        String sqlSelect = "SELECT * FROM Person INNER JOIN Family ON Person.person_id = Family.person_id WHERE Family.person_id=" + personId + "";
        System.out.println(sqlSelect);
        try (
                Connection con = this.connect();
                Statement stmt = con.createStatement()
        ) {
            ResultSet rs = stmt.executeQuery(sqlSelect);

            while (rs.next()) { //Getting the list of related familys ids which are then used to display them
                //System.out.println("relation_id: " + rs.getInt("relation_id"));
                relationlist.put(rs.getInt("relation_id"), rs.getString("title"));
            }

            System.out.println("Displaying related family from table..");

            sqlSelect = "SELECT * FROM Person";
            rs = stmt.executeQuery(sqlSelect);

            while(rs.next()) {
                int rid = rs.getInt("person_id");
                for(int personid : relationlist.keySet()) {
                    if(rid == personid) {
                        System.out.println(
                                "Name: " + rs.getString("first_name") + "\n" +
                                "Surname: " + rs.getString("last_name") + "\n" +
                                "Home address: " + rs.getString("homeaddr") + "\n" +
                                "Date of birth: " + rs.getString("dob") + "\n" +
                                "Title: " + relationlist.get(personid));
                    }
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}

